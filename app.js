const promiseTestModule = () => {
    const promise1 = new Promise(resolve => {
        resolve("response1");
    });

    const promise2 = new Promise(resolve => {
        resolve("response2");
    });

    const promise3 = new Promise(resolve => {
        resolve("response3");
    });

    function outputPromiseResult(promise) {
        return promise.then(result => {
            console.log(result);
            return result;
        });
    }

    promisesArr = [
        outputPromiseResult(promise1),
        outputPromiseResult(promise2),
        outputPromiseResult(promise3)
    ];

    Promise.all(promisesArr)
        .then(result => {
            console.log(`Completed! ${promisesArr.length} promises executed!`);
        })
        .catch(err => {
            console.log(err);
        });
};

promiseTestModule();
